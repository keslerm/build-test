require 'test_helper'
require 'poseidon'

class Build::Test1Test < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Build::Test1::VERSION
  end

  def test_it_does_something_useful
    assert true
  end

  def test_ci_docker_kafka
    # Produce some messages
    producer = Poseidon::Producer.new(["192.168.10.208:9092"], "my_test_producer")
    messages = []
    messages << Poseidon::MessageToSend.new("test", "Message 1")
    messages << Poseidon::MessageToSend.new("test", "Message 2")
    assert producer.send_messages(messages)


    # Simple example test
    consumer = Poseidon::PartitionConsumer.new("my_test_consumer", "192.168.10.208", 9092, 'test', 0, :earliest_offset)
    messages = consumer.fetch
    count = 0
    messages.each do |m|
      count += 1
    end

    assert_equal 2, count
  end
end
